ARG BASE_REGISTRY=registry1.dsop.io
ARG BASE_IMAGE=ironbank/redhat/openjdk/openjdk11
ARG BASE_TAG=1.11

FROM sonarsource/sonar-scanner-cli:4.6 AS build

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

ARG SONAR_SCANNER_HOME=/opt/sonar-scanner
ARG SONAR_SCANNER_VERSION="4.6"
ARG UID=1000
ARG GID=1000
ENV HOME=/tmp \
    XDG_CONFIG_HOME=/tmp \
    SONAR_SCANNER_HOME=${SONAR_SCANNER_HOME} \
    SONAR_USER_HOME=${SONAR_SCANNER_HOME}/.sonar \
    PATH=${SONAR_SCANNER_HOME}/bin:${PATH} \
    SRC_PATH=/tmp/src

WORKDIR /opt

USER root
RUN set -ex \
    && groupadd -r -g ${GID} scanner-cli \
    && useradd -r -u ${UID} -g scanner-cli scanner-cli \
    && dnf update -y \
    && dnf module reset nodejs -y \
    && dnf module install nodejs:14 -y \
    && dnf install -y --nodoc git python3 python3-pip nodejs npm \
    && dnf clean all \
    && rm -rf /var/cache/dnf \
    && sed --in-place --expression="s?securerandom.source=file:/dev/random?securerandom.source=file:/dev/urandom?g" "${JAVA_HOME}/conf/security/java.security" \
    && mkdir -p ${SRC_PATH} \
    && chown -R scanner-cli:scanner-cli ${SRC_PATH} \
    && chmod -s /usr/libexec/openssh/ssh-keysign \
    && chmod +t /usr/bin \
    && chmod +t /usr/libexec \
    && chmod +t /usr/libexec/utempter \
    && chmod +t /usr/libexec/dbus-1 \
    && chmod +t /usr/libexec/p11-kit \
    && chmod +t /usr/libexec/coreutils \
    && chmod +t /usr/libexec/getconf \
    && chmod +t /usr/libexec/awk \
    && chmod +t /usr/libexec/openldap \
    && chmod +t /usr/libexec/selinux \
    && chmod +t /usr/libexec/openssh \
    && chmod +t /usr/libexec/git-core \
    && chmod +t /usr/libexec/git-core/mergetools \
    && chmod +t /usr/local/bin \
    && chmod +t /usr/local/sbin \
    && chmod +t /usr/sbin \
    && chown root:root /usr/bin/write \
    && chown root:root /usr/libexec/utempter/utempter \
    && chown root:root /usr/libexec/dbus-1/dbus-daemon-launch-helper \
    && chown root:root /usr/libexec/openssh/ssh-keysign 

COPY --chown=scanner-cli:scanner-cli --from=build ${SONAR_SCANNER_HOME} ${SONAR_SCANNER_HOME}
COPY --chown=root:root --chmod=0755 scripts/entrypoint.sh /usr/bin/entrypoint.sh

USER scanner-cli

WORKDIR ${SRC_PATH}

ENTRYPOINT ["/usr/bin/entrypoint.sh"]

CMD ["sonar-scanner"]
