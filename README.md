# SonarScanner CLI

SonarScanner is the official scanner used to run code analysis on SonarQube and SonarCloud. This Repository contains official Docker images of SonarScanner CLI.

Warning: These Docker images are not compatible with C/C++/Objective-C projects.

Source repository of the native SonarScanner CLI: https://github.com/SonarSource/sonar-scanner-cli

# Usage and configuration

For information on how to use and configure the image, head over to the Docker section of SonarScanner CLI docs.

Please note that as this image is using less privileges than the official one and as such the run command changes: 

```yaml
docker run \
    --rm \
    -e SONAR_HOST_URL="http://${SONARQUBE_URL}" \
    -e SONAR_LOGIN="myAuthenticationToken" \
    -v "${YOUR_REPO}:/tmp/src/" \
    sonar-scanner-cli
```

# Questions or Feedback

For support questions ("How do I?", "I got this error, why?", ...), please first read the documentation and then head to the SonarSource forum. There are chances that a question similar to yours has already been answered.

Be aware that this forum is a community, so the standard pleasantries ("Hi", "Thanks", ...) are expected. And if you don't get an answer to your thread, you should sit on your hands for at least three days before bumping it. Operators are not standing by. :-)

# Contributing
If you would like to see a new feature, please create a new thread in the forum "Suggest new features".

# License

Copyright 2015-2021 SonarSource.

Licensed under the GNU Lesser General Public License, Version 3.0